import numpy as np
import cv2
import math


#constant values definition
CIRCLE_COLOR = (0, 0, 255)
BOX_COLOR = (255, 0, 0)
LINE_COLOR = (0, 255, 0)
FRAME_WIDTH = 1300
FRAME_HEIGHT = 500
CAMERA_ANGLE = 1.17  # value in radians
DISTANCE_TO_CLOSER_CAR = 6
DISTANCE_TO_FARTHER_CAR = 8
INITIAL_CENTER_COUNT = 1
SAMPLE_COUNT = 3
MIN_OBJECT_HEIGHT = 40
MIN_OBJECT_WIDTH = 50


def sift_tracking(train_image, query_image):

    try:
        MIN_MATCH_COUNT = 7
        img1 = cv2.imread(query_image, 0)  # car image
        img2 = cv2.imread(train_image, 0)  # nextFrame of analyzed video
        # Initiate SIFT detector
        sift = cv2.xfeatures2d.SIFT_create()
        # find the keypoints and descriptors with SIFT
        kp1, des1 = sift.detectAndCompute(img1, None)
        kp2, des2 = sift.detectAndCompute(img2, None)
        FLANN_INDEX_KDTREE = 1
        index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=5)
        search_params = dict(checks=100)
        flann = cv2.FlannBasedMatcher(index_params, search_params)
        matches = flann.knnMatch(des1, des2, k=2)
        # store all the good matches as per Lowe's ratio test.
        good = []
        for m, n in matches:
            if m.distance < 0.7 * n.distance:
                good.append(m)

        if len(good) > MIN_MATCH_COUNT-3:
            src_pts = np.float32([kp1[m.queryIdx].pt for m in good]).reshape(-1, 1, 2)
            dst_pts = np.float32([kp2[m.trainIdx].pt for m in good]).reshape(-1, 1, 2)
            transformation_matrix, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)
            train_height, train_width = img1.shape
            pts = np.float32([[0, 0], [0, train_height - 1], [train_width - 1, train_height - 1], [train_width - 1, 0]]).reshape(-1, 1, 2)
            dst = cv2.perspectiveTransform(pts, transformation_matrix)
            return dst
        else:
            dst = []
            return dst
    
    except Exception as instance:
        dst = []
        return dst
        print instance


def calculate_speed(center_list):
    if (int(center_list[0][0])-int(center_list[-1][0])) > 0:
        field_of_view = 2*DISTANCE_TO_FARTHER_CAR*math.tan(CAMERA_ANGLE/2)
    elif int(center_list[0][0])-int(center_list[-1][0]) < 0:
        field_of_view = 2*DISTANCE_TO_CLOSER_CAR*math.tan(CAMERA_ANGLE/2)
    else:
        print "FOV can not be calculated"

    distance = (int(center_list[0][0])-int(center_list[-1][0]))*field_of_view/FRAME_WIDTH
    time = (-int(center_list[0][2])+int(center_list[-1][2]))*(1.0/capture.get(cv2.CAP_PROP_FPS))
    try:
        speed = int(abs(distance/time*3.6))
    except:
        speed = 0

    return speed


def check_centroid(center_x, center_y, last_centroids,width):
    if len(last_centroids) >= 2:
        threshold_x = abs(last_centroids[-1][0]-last_centroids[-2][0])
        threshold_y = abs(last_centroids[-1][1]-last_centroids[-2][1])
        threshold_w = abs(last_centroids[-1][3] - last_centroids[-2][3])

        if abs(center_x-last_centroids[-1][0]) < threshold_x + 15 and abs(center_y-last_centroids[-1][1]) < threshold_y + 5 \
                and abs(width-last_centroids[-1][3]) < threshold_w + 50:
            return True
        else:
            return False


def morphology_operation(dedicated_frame, kernel_open, kernel_close):

    foreground_mask = mog2_instance.apply(dedicated_frame, None, 0.01)

    foreground_mask = cv2.morphologyEx(foreground_mask, cv2.MORPH_OPEN, kernel_open)
    foreground_mask = cv2.morphologyEx(foreground_mask, cv2.MORPH_CLOSE, kernel_close)

    return foreground_mask


capture = cv2.VideoCapture('Scena2/rzutBoczny3.mp4')

kernel_open = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (4, 4))
kernel_close = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (10, 10))
mog2_instance = cv2.createBackgroundSubtractorMOG2(detectShadows=False)

centers = []
valid_centers = []
frame_counter = 0
valid_contours = []
repetition_number = 0
range_tracking = range(200, FRAME_WIDTH)
road_range = range(200, 400)
previous_length = 0

while capture.isOpened():

    ret, frame = capture.read()

    if ret:
        frame = cv2.resize(frame, (FRAME_WIDTH, FRAME_HEIGHT))
        frame_counter += 1
        frame_mask = morphology_operation(frame,kernel_open, kernel_close)
        im2, contours, hierarchy = cv2.findContours(frame_mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        for contour in contours:
            x, y, width, height = cv2.boundingRect(contour)

            if width > MIN_OBJECT_WIDTH and height > MIN_OBJECT_HEIGHT and frame_counter > 100:

                centerX = x+((width-70)/2)
                centerY = y+(height/2)
                centers.append([centerX, centerY, frame_counter, width])
                if len(centers) == INITIAL_CENTER_COUNT:
                    cropped = frame[y:y+height, x:x+width]
                    cv2.imwrite('testedCar.jpg', cropped)
                elif centerX in range_tracking and centerY in road_range and len(contours) > 0:
                    frame_copy = frame.copy()

                    cv2.imwrite('comparableFrame.jpg', frame_copy)
                    tracking_result = sift_tracking('comparableFrame.jpg', 'testedCar.jpg')

                    if len(tracking_result) != 0:
                            valid_centers.append([centerX, centerY, frame_counter, width])
                    elif len(tracking_result) == 0:

                        if check_centroid(centerX, centerY, valid_centers,width):
                            valid_centers.append([centerX, centerY, frame_counter,width])

                    cropped = frame[y:y + height, x:x + width]
                    cv2.imwrite('testedCar.jpg', cropped)


                if len(valid_centers) >= SAMPLE_COUNT:
                    result = frame
                    if calculate_speed(valid_centers) in range(20, 100):
                        print valid_centers
                        cv2.putText(result, 'V = '+str(calculate_speed(valid_centers))+'km/h', (1100, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 0, 0), 1, cv2.LINE_AA)
                        # cv2.rectangle(result, (x, y), (x+width-300, y+height), BOX_COLOR,3)
                        valid_centers = []
                        centers = []
                        repetition_number = 0
                    else:
                        print valid_centers
                        cv2.putText(result, 'Speed can not be calculated', (5, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 0, 0), 1, cv2.LINE_AA)
                        valid_centers = []
                        centers = []
                        repetition_number = 0
                    cv2.imshow('result', result)
                    cv2.imwrite('testy/result'+str(frame_counter)+'.jpg', result)

        if len(valid_centers) >= 1 and len(valid_centers) < SAMPLE_COUNT:
            repetition_number += 1
            if repetition_number == 5 and len(valid_centers) < SAMPLE_COUNT:
                valid_centers = []
                centers = []
                repetition_number = 0

        cv2.imshow('fr', frame)
        cv2.imshow('frame', frame_mask)

        k = cv2.waitKey(30) & 0xff
        if k == 113:
            break
    else:
        capture.release()
        cv2.destroyAllWindows()
